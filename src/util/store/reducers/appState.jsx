import axios from "axios";
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { merge } from "lodash";

const initialState = {
  initTime: new Date(),
  lastFetchTime: null,
  isAuthenticated: false,
  appReturnUrl: "/",
};

const {
  actions: { set },
  reducer,
} = createSlice({
  name: "appState",
  initialState,
  reducers: {
    set: (state, { payload }) => {
      state = merge(state, payload);
    },
  },
});

export { set };
export default reducer;
