const colorFromString = (str: string) =>
  `hsl(${
    [...str].reduce((a, c) => c.charCodeAt(0) + ((a << 5) - a), 0) % 360
  }, 75%, 20%)`;
export default colorFromString;
