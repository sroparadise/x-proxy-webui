import type { NextPage } from "next";
import { Box } from "@mui/material";

const Home: NextPage = () => {
  return (
    <Box sx={{p: 1}}>DASHBOARD</Box>
  )
};

export default Home;
