import type { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";

const { API_HOST } = process.env;

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (req.method === "POST") {
    const {
      body: { connections },
    } = req;

    const keys = Object.keys(connections);
    let blocklist: any[] = [];

    for await (const key of keys) {
      await axios.post(`${API_HOST}/publish`, {
        context: key,
        operation: "disconnect",
        data: connections[key],
      });

      blocklist = [...blocklist, ...[connections[key]]];
    }

    // use blocklist map with payloads & insert into blocklist db
    // const payload_block = blocklist.map(i => )

    // console.log(payload_block);

    res.status(200).end();
  } else {
    res.status(404).end();
  }
}
