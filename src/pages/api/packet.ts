import type { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";

const { API_HOST } = process.env;

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  if (req.method === "POST") {
    const {
      body: { connections, payload },
    } = req;

    const keys = Object.keys(connections);

    for await (const key of keys) {
      await axios.post(`${API_HOST}/publish`, {
        context: key,
        operation: "message",
        data: {
          direction: payload.direction,
          targets: connections[key],
          opcode: payload?.opcode || false,
          encrypted: payload?.encrypted || false,
          massive: payload?.massive || false,
          data: payload.buffer,
        },
      });
    }

    res.status(200).end();
  } else {
    res.status(404).end();
  }
}
