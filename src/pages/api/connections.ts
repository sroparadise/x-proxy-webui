import type { NextApiRequest, NextApiResponse } from "next";
import axios from "axios";

const { API_HOST } = process.env;

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  const { data } = await axios.post(`${API_HOST}/connections/search`, {
    query: {
      user: {
        wallet: {},
      },
      character: {},
    },
    order: "created desc",
    fields: [
      "character.CharName16",
      "user.StrUserID",
      "user.sec_primary",
      "user.sec_content",
      "user.wallet.silk_own",
      "user.wallet.silk_point",
      "user.wallet.silk_gift",
    ],
  });

  res.status(200).json(data);
}
