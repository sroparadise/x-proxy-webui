import type { NextPage } from "next";
import { useState } from "react";
import {
  Container,
  Box,
  Grid,
  Button,
  Modal,
  FormControl,
  InputLabel,
  Input,
  FormLabel,
  FormControlLabel,
  Radio,
  RadioGroup,
  FormGroup,
  TextField,
  Divider,
  Chip,
  CircularProgress,
  Switch,
  //
} from "@mui/material";
import {
  DataGrid,
  GridColDef,
  GridValueGetterParams,
  GridToolbar,
  //
} from "@mui/x-data-grid";
import useSWR from "swr";
import axios from "axios";
import moment from "moment";
import { toast } from "react-toastify";
import { useConfirm } from "material-ui-confirm";

const fetcher = (url: string) => axios.get(url).then((res) => res.data);

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 640,
  bgcolor: "background.paper",
  border: "1px solid #000",
  borderRadius: 3,
  boxShadow: 24,
  p: 4,
};

const Home: NextPage = () => {
  const { data } = useSWR("/api/connections", fetcher, {
    refreshInterval: 5000,
  });

  const confirm = useConfirm();

  const [state, setState] = useState({
    selected: [],
    filtered: [],
    modalOpen: false,
    payload: {},
    loading: false,
  });

  const columns: GridColDef[] = [
    {
      field: "connection_id",
      headerName: "#",
      flex: 1,
    },
    {
      field: "context",
      headerName: "context",
      flex: 1,
    },
    {
      field: "remote",
      headerName: "IP",
      flex: 1,
    },
    {
      field: "StrUserID",
      headerName: "Username",
      flex: 1,
    },
    {
      field: "character",
      headerName: "Charname",
      flex: 1,
      valueGetter: (params: GridValueGetterParams) =>
        `${params?.value?.CharName16 || "-"}`,
    },
    {
      field: "silk_own",
      headerName: "Credits",
      flex: 1,
    },
    {
      field: "sec_primary",
      headerName: "Rights(p)",
      flex: 1,
    },
    {
      field: "sec_content",
      headerName: "Rights(c)",
      flex: 1,
    },
    {
      field: "created",
      headerName: "Connected Since",
      flex: 1,
      valueGetter: (params: GridValueGetterParams) =>
        `${params?.value ? moment(params.value).fromNow() : "-"}`,
    },
    {
      field: "updated",
      headerName: "Last Update",
      flex: 1,
      valueGetter: (params: GridValueGetterParams) =>
        `${params?.value ? moment(params.value).fromNow() : "-"}`,
    },
  ];

  const rows = () =>
    data?.results?.map((i: any) => ({
      ...i,
      ...i.character,
      ...i.user,
      ...i.user?.wallet,
    })) || [];

  const onSelectionModelChange = (selectionModel: any) => {
    setState({
      ...state,
      selected: selectionModel,
    });
  };

  const getFilteredConnections = () => {
    const loadedConnections: Array<any> = rows() || [];
    const selectedConnections: Array<number> = state.selected;

    const filtered: any = loadedConnections.filter((row: { id: any }): any =>
      selectedConnections.includes(row.id)
    );

    return filtered;
  };

  const onClickSendPacket = () => {
    const filtered = getFilteredConnections();
    setState({
      ...state,
      filtered,
      modalOpen: true,
    });
  };

  const sortConnections = (connections: any[]) =>
    connections.reduce(
      (obj: any, item: { context: string; connection_id: string }) => {
        const { context, connection_id } = item;
        const keys = Object.keys(obj);
        const has_key = keys.includes(context);

        const array = has_key ? obj[context] : [];

        array.push(connection_id);

        return {
          ...obj,
          [context]: array,
        };
      },
      {}
    );

  const onClickConfirmSendPacket = async () => {
    setState({
      ...state,
      loading: true,
    });

    try {
      const filtered = getFilteredConnections();
      const connections = sortConnections(filtered);

      confirm({
        content: (
          <>
            <b>
              This will send following packet to {filtered.length} connections.
            </b>
            <p style={{color: "orange"}}>
              Be aware that sending to different modules at same time will cause
              the clients to crash.
            </p>
          </>
        ),
      }).then(async () => {
        await axios.post("/api/packet", {
          connections,
          payload: state.payload,
        });

        toast.success(`Packet was sent successfully!`);
      });
    } catch (e) {
      toast.error("Failed to send packet!");
    } finally {
      setState({
        ...state,
        loading: false,
      });
    }
  };

  const onClickDisconnect = async () => {
    setState({
      ...state,
      loading: true,
    });
    try {
      const filtered = getFilteredConnections();
      const connections = sortConnections(filtered);

      confirm({
        description: `This will disconnect ${filtered.length} account(s) from the game.`,
      }).then(async () => {
        await axios.post("/api/disconnect", { connections });
        toast.success("Disconnected successfully!");
      });
    } catch (e) {
      toast.error("Failed to disconnect!");
    } finally {
      setState({
        ...state,
        loading: false,
      });
    }
  };

  return (
    <>
      <div style={{ display: "flex", height: "100%" }}>
        <div style={{ flexGrow: 1 }}>
          <DataGrid
            autoHeight
            components={{
              Toolbar: () => (
                <Grid container>
                  <Grid item xs={6}>
                    <GridToolbar />
                  </Grid>
                  <Grid xs={6} item>
                    <Grid
                      sx={{
                        padding: "5px",
                      }}
                      container
                      justifyContent="flex-end"
                      alignItems="center"
                    >
                      {state.selected.length > 0 && (
                        <>
                          {state.selected.length} connection(s) selected...
                          <Button
                            sx={{
                              marginLeft: "5px",
                            }}
                            variant="outlined"
                            color="error"
                            onClick={onClickDisconnect}
                          >
                            DISCONNECT
                          </Button>
                          <Button
                            disabled={state.loading}
                            sx={{
                              marginLeft: "5px",
                            }}
                            variant="outlined"
                            onClick={onClickSendPacket}
                          >
                            SEND PACKETS
                          </Button>
                          {/* <Button
                            disabled={state.loading}
                            sx={{
                              marginLeft: "5px",
                            }}
                            variant="outlined"
                            onClick={onClickStartBot}
                          >
                            START BOTTING
                          </Button> */}
                        </>
                      )}
                    </Grid>
                  </Grid>
                </Grid>
              ),
            }}
            rows={rows()}
            columns={columns}
            checkboxSelection
            disableSelectionOnClick
            onSelectionModelChange={onSelectionModelChange}
          />
        </div>
      </div>

      <Modal
        open={state.modalOpen}
        onClose={() => setState({ ...state, modalOpen: false })}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Grid container>
            <Grid item xs={12}>
              <InputLabel sx={{ pb: 2 }}>Selected Connections:</InputLabel>

              {state.filtered.map((i: any, key: number) => (
                <Chip
                  sx={{
                    marginBottom: "5px",
                    marginRight: "5px",
                  }}
                  key={key}
                  label={`${i.context}: ${i?.CharName16 || i.connection_id}`}
                />
              ))}
            </Grid>

            <Grid item sx={{ mt: 1 }} xs={5}>
              <InputLabel htmlFor="opcode">OPCODE</InputLabel>
              <TextField
                sx={{ width: "100%" }}
                type="number"
                variant="outlined"
                placeholder="28705"
                onChange={({ target: { value } }) =>
                  setState({
                    ...state,
                    payload: {
                      ...state.payload,
                      opcode: parseInt(value),
                    },
                  })
                }
              />
            </Grid>

            <Grid sx={{ width: "100%", pl: 1 }} item xs={7}>
              <FormControl component="fieldset">
                <RadioGroup
                  sx={{ paddingTop: "38px", pl: 2 }}
                  row
                  aria-label="direction"
                  id="direction"
                  defaultValue="remote"
                  name="direction"
                  onChange={({ target: { value } }) =>
                    setState({
                      ...state,
                      payload: {
                        ...state.payload,
                        direction: value,
                      },
                    })
                  }
                  // onChange={handleRadioChange}
                >
                  <FormControlLabel
                    value="remote"
                    control={<Radio />}
                    label="Send to Server"
                  />
                  <FormControlLabel
                    value="client"
                    control={<Radio />}
                    label="Send to Client"
                  />
                </RadioGroup>
              </FormControl>
            </Grid>

            <Grid item sx={{ mt: 1 }} xs={6}>
              <FormGroup>
                <FormControlLabel
                  control={
                    <Switch
                      defaultChecked={false}
                      onChange={({ target: { checked } }) =>
                        setState({
                          ...state,
                          payload: {
                            ...state.payload,
                            encrypted: checked,
                          },
                        })
                      }
                    />
                  }
                  label="Mark packet as Encrypted"
                />
              </FormGroup>
            </Grid>
            <Grid item sx={{ mt: 1 }} xs={6}>
              <FormGroup>
                <FormControlLabel
                  control={
                    <Switch
                      defaultChecked={false}
                      onChange={({ target: { checked } }) =>
                        setState({
                          ...state,
                          payload: {
                            ...state.payload,
                            massive: checked,
                          },
                        })
                      }
                    />
                  }
                  label="Mark packet as Massive"
                />
              </FormGroup>
            </Grid>

            <Grid item sx={{ mt: 1 }} xs={12}>
              <InputLabel htmlFor="opcode">DATA</InputLabel>
              <TextField
                sx={{ width: "100%", mb: 2 }}
                type="textarea"
                aria-describedby="opcode-helper-text"
                variant="outlined"
                placeholder={`1,166,98,221,3,252,255,44,0`}
                onChange={({ target: { value } }) =>
                  setState({
                    ...state,
                    payload: {
                      ...state.payload,
                      buffer: value.split(",").map((i) => parseInt(i)),
                    },
                  })
                }
              />
            </Grid>
          </Grid>

          <Button
            disabled={state.loading}
            onClick={onClickConfirmSendPacket}
            sx={{ width: "100%", mt: 1 }}
            variant="contained"
          >
            {state.loading ? <CircularProgress /> : "SEND"}
          </Button>
        </Box>
      </Modal>
    </>
  );
};

export default Home;
