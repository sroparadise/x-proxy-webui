import { Suspense, useState } from "react";
import Head from "next/head";
import { AppProps } from "next/app";
import { ThemeProvider } from "@mui/material/styles";
import { CssBaseline, Box, Paper, useMediaQuery } from "@mui/material";
import { CacheProvider, EmotionCache } from "@emotion/react";
import theme from "../util/theme";
import createEmotionCache from "../util/createEmotionCache";
import Navigator from "../components/Navigator";
import Header from "../components/Header";
import { ToastContainer } from "react-toastify";
import { ConfirmProvider } from "material-ui-confirm";
import LinearProgress from "@mui/material/LinearProgress";
import "../styles/globals.css";
import "react-toastify/dist/ReactToastify.css";

interface CoreAppProps extends AppProps {
  emotionCache?: EmotionCache;
}

const drawerWidth = 256;
const clientSideEmotionCache = createEmotionCache();

export default function AdminApp(props: CoreAppProps) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props;

  const [mobileOpen, setMobileOpen] = useState(false);
  const isSmUp = useMediaQuery(theme.breakpoints.up("sm"));

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  return (
    <CacheProvider value={emotionCache}>
      <Head>
        <meta name="viewport" content="initial-scale=1, width=device-width" />
        <link rel="icon" href="favicon.ico" />
        <base href="/" />
        <title>SROSUITE</title>
      </Head>
      <ThemeProvider theme={theme}>
        <ConfirmProvider>
          <Suspense fallback={<LinearProgress color="secondary" />}>
            <Box sx={{ display: "flex", minHeight: "100vh" }}>
              <CssBaseline />
              <Box
                component="nav"
                sx={{ width: { sm: drawerWidth }, flexShrink: { sm: 0 } }}
              >
                {isSmUp ? null : (
                  <Navigator
                    PaperProps={{ style: { width: drawerWidth } }}
                    variant="temporary"
                    open={mobileOpen}
                    onClose={handleDrawerToggle}
                  />
                )}
                <Navigator
                  PaperProps={{ style: { width: drawerWidth } }}
                  sx={{ display: { sm: "block", xs: "none" } }}
                />
              </Box>
              <Box sx={{ flex: 1, display: "flex", flexDirection: "column" }}>
                <Header onDrawerToggle={handleDrawerToggle} />
                <Box
                  component="main"
                  sx={{ flex: 1, py: 4, px: 3, bgcolor: "#777" }}
                >
                  <Paper>
                    <Component {...pageProps} />
                  </Paper>
                </Box>
              </Box>
            </Box>

            <ToastContainer
              position="bottom-center"
              autoClose={5000}
              closeOnClick
              pauseOnFocusLoss
              draggable
              pauseOnHover
            />
          </Suspense>
        </ConfirmProvider>
      </ThemeProvider>
    </CacheProvider>
  );
}

//
