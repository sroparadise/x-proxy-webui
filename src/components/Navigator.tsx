import * as React from "react";
import Divider from "@mui/material/Divider";
import Drawer, { DrawerProps } from "@mui/material/Drawer";
import List from "@mui/material/List";
import Box from "@mui/material/Box";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";
import HomeIcon from "@mui/icons-material/Home";
import PeopleIcon from "@mui/icons-material/People";
import DnsRoundedIcon from "@mui/icons-material/DnsRounded";
import PublicIcon from "@mui/icons-material/Public";
import SettingsEthernetIcon from "@mui/icons-material/SettingsEthernet";

import SettingsIcon from "@mui/icons-material/Settings";
import Link from "./Link";
import Image from "next/image";
import { useRouter } from "next/router";
import { BarChart, Diamond, DirectionsRun, ShoppingCart } from "@mui/icons-material";

const categories = [
  {
    id: "Database",
    children: [
      {
        id: "Connections",
        icon: <SettingsEthernetIcon />,
        href: "/connections",
      },
      { id: "Accounts", icon: <PeopleIcon />, href: "/accounts" },
      { id: "Characters", icon: <DirectionsRun />, href: "/characters" },
    ],
  },
  {
    id: "Server",
    children: [
      { id: "Settings", icon: <SettingsIcon />, href: "/settings" },
      { id: "Statistics", icon: <BarChart />, href: "/statistics" },
      { id: "Mall Editor", icon: <ShoppingCart />, href: "/webmall" },
      { id: "Items List", icon: <Diamond />, href: "/items" },
    ],
  },
];

const item = {
  py: "2px",
  px: 3,
  color: "rgba(255, 255, 255, 0.7)",
  "&:hover, &:focus": {
    bgcolor: "rgba(255, 255, 255, 0.08)",
  },
};

const itemCategory = {
  boxShadow: "0 -1px 0 rgb(255,255,255,0.1) inset",
  py: 1.5,
  px: 3,
};

export default function Navigator(props: DrawerProps) {
  const { ...other } = props;
  const router = useRouter();

  return (
    <Drawer variant="permanent" {...other}>
      <List disablePadding>
        <ListItem
          disablePadding
          disableGutters
          sx={{
            ...item,
            ...itemCategory,
            padding: "15px",
            fontSize: 22,
            color: "#fff",
            textAlign: "left",
            backgroundColor: "#000 !important",
          }}
        >
          <Image alt={`SROSUITE`} src={`/LOGO.png`} width={45} height={45} />{" "}
          <span>SROSUITE</span>
        </ListItem>
        <Link href="/">
          <ListItem sx={{ ...item, ...itemCategory }}>
            <ListItemIcon>
              <HomeIcon />
            </ListItemIcon>
            <ListItemText>Dashboard</ListItemText>
          </ListItem>
        </Link>
        {categories.map(({ id, children }) => (
          <Box key={id}>
            <ListItem sx={{ py: 2, px: 3 }}>
              <ListItemText sx={{ color: "#fff" }}>{id}</ListItemText>
            </ListItem>
            {children.map(({ id: childId, icon, href }) => (
              <Link key={childId} href={href}>
                <ListItem disablePadding>
                  <ListItemButton selected={router.asPath === href} sx={item}>
                    <ListItemIcon>{icon}</ListItemIcon>
                    <ListItemText>{childId}</ListItemText>
                  </ListItemButton>
                </ListItem>
              </Link>
            ))}
            <Divider sx={{ mt: 2 }} />
          </Box>
        ))}
      </List>
    </Drawer>
  );
}
